package fr.tokidev.tp4

enum class GameStatus {
    UNDER,
    EQUAL,
    OVER
}