package fr.tokidev.tp4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import fr.tokidev.tp4.databinding.ActivityScoreBinding

class ScoreActivity : AppCompatActivity() {

    private lateinit var binding: ActivityScoreBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_score)

        var scores: ArrayList<Int> = intent.getIntegerArrayListExtra("scores")!!

        if (scores.size > 0) {
            val sortedScores = scores.sorted()
            if (sortedScores.size > 0) {
                binding.nb1.setText("1 - ${sortedScores!![0]}")
            } else {
                binding.nb1.setText("")
            }

            if (sortedScores.size > 1) {
                binding.nb2.setText("2 - ${sortedScores!![1]}")
            } else {
                binding.nb2.setText("")
            }

            if (sortedScores.size > 2) {
                binding.nb3.setText("3 - ${sortedScores!![2]}")
            } else {
                binding.nb3.setText("")
            }
        } else {
            binding.nb1.setText("")
            binding.nb2.setText("")
            binding.nb3.setText("")
        }

        binding.closeButton.setOnClickListener {
            finish()
        }
    }
}