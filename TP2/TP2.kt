enum class GameStatus {
    UNDER, EQUAL, OVER
}

class Game {
    companion object {
        var nbRound: Int = 0
        var finalValue: Int = 0

        fun reset() {
            nbRound = 0
            finalValue = (0..100).random()
        }

        fun getValue(): Int {
            nbRound++
            return readLine()!!.toInt()
        }

        fun checkValue(value: Int) : GameStatus {            
            when {
                value < finalValue -> return GameStatus.UNDER
                value > finalValue -> return GameStatus.OVER
                else -> return GameStatus.EQUAL
            }
        }

        fun showResult(result: GameStatus) {
            if (result == GameStatus.OVER) {
                println("Secret value is lower")
            } else if (result == GameStatus.UNDER) {
                println("Secret value is higher")                
            }
        }
    }    
}

fun main() {
    Game.reset()
    
    var enteredValue: Int
    var result: GameStatus?

    println("Welcome to the game ! Try to guess the number between 0 & 100 :")        
    enteredValue = Game.getValue()   
    result = Game.checkValue(enteredValue)    

    while (result!! != GameStatus.EQUAL) {
        Game.showResult(result)

        enteredValue = Game.getValue()
        result = Game.checkValue(enteredValue)
    }

    println("Congratulations !!! You found in ${Game.nbRound} tries !")
     
}