package fr.tokidev.saleads

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.databinding.DataBindingUtil
import fr.tokidev.saleads.databinding.ActivityAddBinding
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class AddActivity : AppCompatActivity() {
    private lateinit var binding: ActivityAddBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_add)

        binding.createButton.setOnClickListener {
            val title = binding.addTitle.text.toString()
            val desc = binding.addDescription.text.toString()
            val price = binding.addPrice.text.toString()

            val newSale: SaleAdModel = SaleAdModel(title, desc, price)

            SaleAdsApi.retrofitService.createSalesAd(newSale).enqueue (
                object: Callback<Void> {
                    override fun onResponse(call: Call<Void>, response: Response<Void>) {
                        finish()
                    }

                    override fun onFailure(call: Call<Void>, t: Throwable) {
                        Log.e("API", "Failure : " + t.message)
                        finish()
                    }

                }
            )
        }

        binding.cancelButton.setOnClickListener {
            finish()
        }
    }
}