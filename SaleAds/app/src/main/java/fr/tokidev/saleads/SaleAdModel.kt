package fr.tokidev.saleads

/*{
    "id": 1,
    "title": "title Greg 1",
    "dateCreated": "2020-10-10T21:29:39Z",
    "price": 5.0,
    "lastUpdated": "2020-10-10T21:29:39Z",
    "illustrations": [{ "id": 2 }, { "id": 1 }, { "id": 3 }],
    "status": true,
    "description": "Nice description",
    "author": { "id": 1 }
}*/

data class SaleAdModel(
    val title: String,
    val description: String,
    val price: String
)