package fr.tokidev.saleads

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.MutableLiveData
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

/**
 * A fragment representing a list of Items.
 */
class SaleAdFragment : Fragment() {

    private var columnCount = 1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        arguments?.let {
            columnCount = it.getInt(ARG_COLUMN_COUNT)
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_item_list, container, false)

        SaleAdsApi.retrofitService.getSalesAd("100").enqueue(
            object: Callback<List<SaleAdModel>> {
                override fun onResponse(
                    call: Call<List<SaleAdModel>>,
                    response: Response<List<SaleAdModel>>
                ) {
                    // Set the adapter
                    if (view is RecyclerView && response.body() != null) {
                        with(view) {
                            layoutManager = when {
                                columnCount <= 1 -> LinearLayoutManager(context)
                                else -> GridLayoutManager(context, columnCount)
                            }

                            adapter = MySaleAdRecyclerViewAdapter(response.body()!!)
                        }
                    }
                }

                override fun onFailure(call: Call<List<SaleAdModel>>, t: Throwable) {
                    Log.e("API", "Failure : " + t.message)
                }

            }
        )

        return view
    }

    companion object {

        // TODO: Customize parameter argument names
        const val ARG_COLUMN_COUNT = "column-count"

        // TODO: Customize parameter initialization
        @JvmStatic
        fun newInstance(columnCount: Int) =
            SaleAdFragment().apply {
                arguments = Bundle().apply {
                    putInt(ARG_COLUMN_COUNT, columnCount)
                }
            }
    }
}