package fr.tokidev.saleads

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.http.*

private const val BASE_URL = "http://192.168.88.227:8095/api/"

private val moshi = Moshi.Builder()
    .add(KotlinJsonAdapterFactory())
    .build()

private val retrofit = Retrofit.Builder()
    .addConverterFactory(MoshiConverterFactory.create(moshi))
    .baseUrl(BASE_URL)
    .build()

interface SaleAdsApiService {

    @Headers("Accept: application/json")
    @GET("salesad")
    fun getSalesAd(@Query("max") max: String):
            Call<List<SaleAdModel>>

    @Headers("Accept: application/json")
    @POST("salesad")
    fun createSalesAd(@Body saleAdModel: SaleAdModel):
            Call<Void>
}

object SaleAdsApi {
    val retrofitService: SaleAdsApiService by lazy { retrofit.create(SaleAdsApiService::class.java) }
}