package fr.tokidev.tp5

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        ApiService.retrofitService.getSalesAd().enqueue(
                object: Callback<List<TodoModel>> {
                    override fun onResponse(call: Call<List<TodoModel>>, response: Response<List<TodoModel>>) {
                        Log.i("API", "Success : ${response.body()!!.size} todo parsed.")
                    }

                    override fun onFailure(call: Call<List<TodoModel>>, t: Throwable) {
                        Log.e("API", "Failure : " + t.message)
                    }

                }
        )
    }
}