package fr.tokidev.tp5

/*
  {
    "userId": 1,
    "id": 1,
    "title": "delectus aut autem",
    "completed": false
  }
 */
data class TodoModel (
    val userId: Int,
    val id: Int,
    val title: String,
    val completed: Boolean
)