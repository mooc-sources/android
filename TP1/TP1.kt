fun main() {
    // Ex. 1
    val myInt: Int = 3
    val myString: String = "This is a string"
    println("Int : ${myInt} - String : ${myString}")

    // Ex. 2
    println("3 + 2 = ${add(3, 2)}")

    // Ex. 3
    val titles = mutableListOf<String>()
    titles.add("Title 1")
    titles.add("Title 2")
    println("Titles : ${titles}")

    // Ex. 4
    val b1: Book = Book("Title 1", "Author 1")
    val b2: Book = Book("Title 2", "Author 2")

    val library = mutableListOf<Book>()
    library.add(b1)
    library.add(b2)

    for (b: Book in library) {
        println(b.title)
    }
}

fun add(a: Int, b: Int) : Int {
    return a + b
}

class Book (val title: String, val author: String)