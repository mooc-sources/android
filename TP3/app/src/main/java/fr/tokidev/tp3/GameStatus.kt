package fr.tokidev.tp3

enum class GameStatus {
    UNDER,
    EQUAL,
    OVER
}