package fr.tokidev.tp3

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import fr.tokidev.tp3.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private var scores = mutableListOf<Int>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        Game.sharedInstance.init()

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        binding.playButton.setOnClickListener {
            val enteredValue: Int = Integer.parseInt(binding.numberEdit.text.toString())
            val result: GameStatus = Game.sharedInstance.checkValue(enteredValue)

            if (result == GameStatus.OVER) {
                binding.resultTextView.setText(getString(R.string.secret_value_is_lower))
            } else if (result == GameStatus.UNDER) {
                binding.resultTextView.setText(getString(R.string.secret_value_is_higher))
            } else {
                binding.resultTextView.setText("")

                val nbRound: Int = Game.sharedInstance.getNbRound()
                scores.add(nbRound)

                val toastTxt: String = getString(R.string.congratulations_you_found, nbRound)
                val duration = Toast.LENGTH_SHORT

                val toast = Toast.makeText(applicationContext, toastTxt, duration)
                toast.show()
            }

            binding.numberEdit.setText("")
        }

        binding.resetButton.setOnClickListener {
            binding.numberEdit.setText("")
            binding.resultTextView.setText(getString(R.string.enter_a_number_to_play_the_game))

            Game.sharedInstance.reset()
        }
    }
}