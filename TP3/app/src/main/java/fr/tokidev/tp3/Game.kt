package fr.tokidev.tp3

class Game {
    companion object {
        val sharedInstance = Game()
    }

    private var nbRound: Int = 0
    private var finalValue: Int = 0

    fun init() {
        finalValue = (0..100).random()
    }

    fun reset() {
        nbRound = 0
        finalValue = (0..100).random()
    }

    fun checkValue(value: Int) : GameStatus {
        nbRound++

        when {
            value < finalValue -> return GameStatus.UNDER
            value > finalValue -> return GameStatus.OVER
            else -> return GameStatus.EQUAL
        }
    }

    fun getNbRound(): Int {
        return this.nbRound
    }
}